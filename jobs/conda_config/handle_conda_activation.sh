#!/bin/bash

# use ENV_NAME that is passed down from job_template.sh, otherwise use "base"
ENV_NAME="${ENV_NAME:-base}"

module purge
module load devel/miniconda/4.9.2

source ~/.bashrc
# check, if conda can be loaded via custom function
if command -v conda_init >/dev/null 2>&1; then
    echo "conda_init is available."
else
    echo "conda_init is not available. Adding to .bashrc..."
    cat jobs/conda_config/custom_conda_init.sh >> ~/.bashrc
    echo "conda_init has been added to .bashrc and can be used from now on."
fi

source ~/.bashrc
conda_init

# Check if $ENV_NAME environment exists
if conda env list | grep -qi "^$ENV_NAME\s"; then
  echo "Conda environment '$ENV_NAME' already exists, loading it..."
  conda activate "$ENV_NAME"

  echo "Conda environment '$ENV_NAME' loaded."
else
  # Create $ENV_NAME environment if it does not exist
  echo "Creating Conda environment '$ENV_NAME'..."
  conda create -n "$ENV_NAME"
  echo "Conda environment '$ENV_NAME' created. Installing dependencies..."

  conda activate "$ENV_NAME"
  #
  # Instructions for installing dependencies here
  #
  # e.g.: install from env.yml (do not forget the --name flag, otherwise it will install to the specified env in the key name of the env.yml file)
  conda env update --name "$ENV_NAME" --file env.yml --prune
  # or: conda install -c pytorch pytorch torchvision torchaudio cudatoolkit=11.1 -c nvidia
  # or: pip install -r requirements.txt
  # done with dependencies

  echo "Conda environment '$ENV_NAME' created, loaded and dependencies installed."
fi
