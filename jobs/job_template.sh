#!/bin/bash
#SBATCH --time=03:00:00
#SBATCH --mem=256gb
#SBATCH --job-name=aiss_cv
#SBATCH --gres=gpu:2
# above: comments above for sbatch configuration (can be overriden in sbatch cli command)

#
# Conda environment name
# - will use "base" if not specified
# - will create a new environment if the specified environment does not exist
# - configure dependency installation in jobs/conda_config/handle_conda_activation.sh
#
export ENV_NAME="yolov8"
#
# Configuration variables (simplify configuration of jobs)
#
MODEL_FILE=yolov8n.yaml
WEIGHTS_FILE=yolov8n.pt
DATA_FILE=./data/data_v3_0_final/data.yaml
IMG_SIZE=640
EPOCHS=300
BATCH_SIZE=128
CACHE_ENABLED=true
# amount of GPU devices to use (specific to GPU usage)
AMOUNT_DEVICES=2

# Generate device IDs list
DEVICE_IDS=$(seq -s "," 0 $((AMOUNT_DEVICES-1)))


# generated names
# Extract version number from data file using regex
VERSION=$(echo "$DATA_FILE" | grep -oP 'data_v\K\d+_\d+')

# Generate ending part of the name based on weights file
ENDING=""
if [[ $WEIGHTS_FILE == false ]]; then
    ENDING="noStartWeights"
elif [[ $WEIGHTS_FILE == yolo* ]]; then
    ENDING=$(basename "$WEIGHTS_FILE" .pt)
    ENDING=${ENDING#"yolo"}
    ENDING=${ENDING//_/}
    ENDING=${ENDING^}
else
    ENDING=$(basename "$WEIGHTS_FILE" .pt)
    ENDING=${WEIGHTS_FILE^}
fi

# Extract configuration file name without extension
CONFIG_NAME=$(basename "$MODEL_FILE" .yaml)

# Remove "yolo" from the beginning of the configuration file name
CONFIG_NAME=${CONFIG_NAME#"yolo"}

# Generate name
NAME="v${VERSION}_${IMG_SIZE}x_${BATCH_SIZE}b_${EPOCHS}e_${ENDING}W_${CONFIG_NAME}-model"


#
# CONDA SETUP
#
# make sure conda is available and properly set up
source jobs/conda_config/handle_conda_activation.sh

# Run the command to get the active environment information
env_info=$(conda info --json)

# Check if the environment_info contains the "active_prefix" key
if [[ $env_info == *"active_prefix"* ]]; then
    # Extract the active environment name from the environment_info
    active_env=$(echo "$env_info" | jq -r '.active_prefix' | awk -F'/' '{print $NF}')
    echo "Conda environment '$active_env' is active."
else
    echo "Conda environment is not active."
fi
# end of conda setup

echo "Setup complete. Starting job... 🚀"

# job command
yolo detect train \
    model=$MODEL_FILE \
    pretrained=$WEIGHTS_FILE \
    data=$DATA_FILE \
    imgsz=$IMG_SIZE \
    epochs=$EPOCHS \
    batch-size=$BATCH_SIZE \
    device=$DEVICE_IDS \
    cache=$CACHE_ENABLED \
    name=$NAME

echo "Job finished. ✨🎉"