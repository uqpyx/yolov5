- [About this file](#about-this-file)
- [Getting Started](#getting-started)
  - [First steps](#first-steps)
  - [Structure of the repository](#structure-of-the-repository)
  - [Data config file (data.yaml)](#data-config-file-datayaml)
  - [Starting a job](#starting-a-job)
  - [Creating a new job script](#creating-a-new-job-script)
    - [What the job template script does](#what-the-job-template-script-does)
- [Results](#results)

# About this file

This file explains a few useful features and the way scripts have been added to use the hpc cluster. It will not document details regarding yolov5 (see it's [documentation here](https://docs.ultralytics.com/yolov5/)) It will not go into details that are already explained in the [documentation of the bwUniCluster2.0](https://wiki.bwhpc.de/e/BwUniCluster2.0).

# Getting Started

## First steps

Clone this repository into the cluster, or onto your host machine and copy it to the cluster (via scp, sftp or rsync). You can either already download the data (e.g. from roboflow) and put it into the `data/` folder, or do it later. Remember to adjust the rsync if you want to sync another specific folder.

Ideally, you can already install the required dependencies to a conda environment in the hpc and specify the environment name in the job script. This way, the dependencies won't have to be installed during a job. Read more about it in the [instructions for the hpc scripts](https://git.scc.kit.edu/aiss_cv/hpc_enabled_frameworks/hpc_scripts/-/blob/main/README.md#configuration-variables).

> The data should contain a minimum of `vx_x` of versioning. This is important, because the data file config will be named after the data version. This makes it easier to keep track of the data and the results.

```bash
# Clone the repo from https://git.scc.kit.edu/aiss_cv/hpc_enabled_frameworks/yolov5.git
git clone https://git.scc.kit.edu/aiss_cv/hpc_enabled_frameworks/yolov8.git
cd yolov8

# copy the repo to the cluster if not cloned there directly
rsync -rPzhv --info=progress2 yolov8/ <username>@bwunicluster.scc.kit.edu:~/yolov5
```

## Structure of the repository

```bash
.
├── README.md # this file
├── data # this file should contain all the data
│   └── data_v3_0_final # should contain versioning of type vx_x and be in sync with data versioning on all tools
│       ├── data.yaml # config file for data
│       ├── test
│       ├── train
│       └── valid
└── jobs
    ├── conda_config # used for conda configs
    └── job_template.sh # template for job scripts
```

## Data config file (data.yaml)

Here is, what the data config file could look like.

```yaml
path: ../data/data_v3_0_final/ # root path of data
train: ./train/images # relative to root path
val: ./valid/images # relative to root path
test: ./test/images # relative to root path

nc: 31
# an excerpt of the class names (nr should match nc)
names:
  [
    "black_short",
    "black_stick_long",
    "blue_long",
    "blue_short",
    "wheel",
    "white_45",
  ]
```

## Starting a job

The hpc cluster uses the [Slurm Workload Manager](https://slurm.schedmd.com/documentation.html) to manage jobs. To start a job, one creates a job script and submits it to the cluster. The job script is a bash script that contains the commands to be executed on the cluster. There is a job script template in the `jobs` directory. See [the hpc scripts repository for more details](https://git.scc.kit.edu/aiss_cv/hpc_enabled_frameworks/hpc_scripts.git).

> ⚠️ Remember to always call the jobs from the root directory of the repository, otherwise the relative paths will not be correct.

```bash
sbatch -p <partition> jobs/<job_script>.sh
```

To see available partitions, use `sinfo`:

```bash
sinfo_t_idle
```

## Creating a new job script

To create a new job script, simply copy the template and adjust it to your needs. It is highly recommended to create a separate job script for each task, as this makes it easier to keep track of the jobs and their progress. Later, they can be removed if the job was successful.

```bash
cp jobs/template.sh jobs/<job_script>.sh
```

The job template contains the most important options as variables at the top of the script:

```bash
#...
# this is the conda environemnt name
export ENV_NAME="yolov8"
# Configuration variables
MODEL_FILE=yolov8n.yaml
WEIGHTS_FILE=yolov8n.pt
DATA_FILE=./data/data_v3_0_final/data.yaml
IMG_SIZE=640
EPOCHS=300
BATCH_SIZE=128
CACHE_ENABLED=true
AMOUNT_DEVICES=2

#...
```

Use them, to adjust the yolov5 job. Here is a short explanation of them:

| Variable         | Description                                                                                                                                                                                                       |
| ---------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `MODEL_FILE`     | The path to the model config file.                                                                                                                                                                                |
| `WEIGHTS_FILE`   | The path to the weights file. Examples: `yolov8s.pt`, `false`                                                                                                                                                     |
| `DATA_FILE`      | The path to the data file config (recommendation: create a separate one for each data version and use versioning for them in the labeling tool of your choice, e.g. roboflow).                                    |
| `IMG_SIZE`       | The size of the input images.                                                                                                                                                                                     |
| `EPOCHS`         | The number of epochs to train.                                                                                                                                                                                    |
| `BATCH_SIZE`     | The batch size.                                                                                                                                                                                                   |
| `CACHE_ENABLED`  | Whether to use the cache or not.                                                                                                                                                                                  |
| `AMOUNT_DEVICES` | The amount of devices to use. ⚠️ These also have to be changed in the comments above that let slurm know, how many ressources we request (and is therefore the only attribute that needs to be set in two places) |

The name of the run will be deferred from these options. It will follow this structure: `<data_version_number>_<imgSize>x_<batchSize>b_<epochs>e_<weights>W_<shortFormModel>-model`. For example: `v1_0_640x_128b_4e_V5sW_v5s-model`. This makes it easy to identify the run and its parameters.

### What the job template script does

> The script template is meant to be used by only changing the parameter variables at its top. This section solely serves as an explanation of what the script does. Changes of the script logic should be done with caution and at the developers discretion.

The `jobs/job_template.sh` script is a template for any new jobs. In it, the hpc checks for `conda` and loads the software module for conda (see [software modules on hpc](https://wiki.bwhpc.de/e/BwUniCluster2.0/Software_Modules)) if it is not yet available. Then, it checks for the necessary dependencies and installs them, if they are not yet available in the conda environment. Then, the conda environment is loaded and training starts with the given parameters as [described above](#creating-a-new-job-script). Read more about the job script template and its individual sections in the [hpc scripts repository](https://git.scc.kit.edu/aiss_cv/hpc_enabled_frameworks/hpc_scripts.git).

# Results

Results produced by the [BwUniCluster2.0](https://wiki.bwhpc.de/e/BwUniCluster2.0) are kept in a separate repo here: [https://git.scc.kit.edu/aiss_cv/yolo_models/yolov8.git](https://git.scc.kit.edu/aiss_cv/yolo_models/yolov8.git).
